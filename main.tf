provider "digitalocean" {}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "digitalocean_ssh_key" "default" {
  name       = "Terraform Droplet Example"
  public_key = "${tls_private_key.example.public_key_openssh}"
}

resource "digitalocean_droplet" "web" {
  image    = "ubuntu-18-04-x64"
  name     = "terraform-droplet-example"
  region   = "lon1"
  size     = "s-1vcpu-1gb"
  ssh_keys = ["${digitalocean_ssh_key.default.fingerprint}"]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "root"
      private_key = "${tls_private_key.example.private_key_pem}"
      host        = "${digitalocean_droplet.web.ipv4_address}"
    }
    inline = [
      "whoami"
    ]
  }
}